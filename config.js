module.exports = {
    // *************
    // for updates from non-official servers, e.g. company repos, vendor repos
    // *************
    // hostRules: [
    //     {
    //         hostType: 'maven',
    //         matchHost: 'https://your_artifactory_1.example.test/maven-yourcorp',
    //         username: process.env.ARTIFACTORY_USERNAME,
    //         password: process.env.ARTIFACTORY_PASSWORD,
    //     }
    // ],
    repositories: [
        'cy4n/renovate-runner-demo',
        'cy4n/renovate-demo',
    ],
    gitAuthor: "RenovateDemo <chris+renovate@clowncomputing.de>",
    onboarding: true,
    baseDir: process.env.CI_PROJECT_DIR + '/renovate',
    endpoint: process.env.CI_API_V4_URL,
    platform: 'gitlab',
    onboardingConfig: {
        'extends': [
            'config:base',
        ],
    },
    optimizeForDisabled: true,
    repositoryCache: true,
    requireConfig: true,
    ignorePrAuthor: true,
    extends: [
        'github>whitesource/merge-confidence:beta',
    ],
    logFile: 'renovate-log.ndjson',
    logFileLevel: 'debug',
    logLevel: 'debug'
};
